// Lab5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"

HANDLE openFile(int fileNum)
{
	HANDLE h = INVALID_HANDLE_VALUE;
	TCHAR* filePath;
	if (fileNum == 1){
		filePath = _T("d:\\lab5\\1.txt");
	}
	else{
		filePath = _T("d:\\lab5\\2.txt");
	}
	
	bool b = false;
	
	h = CreateFile(filePath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	//fullPath = NULL;
	return h;
}

BYTE* readFile(HANDLE h, DWORD dwSize)
{
	bool b = true;
	//int resultSize = 100000 * 50;
	BYTE* result = (BYTE*)malloc(dwSize);

	DWORD dwCount = 0;

	HANDLE hMap;
	LPVOID pMap;
	//DWORD dwSize;

	if (h != INVALID_HANDLE_VALUE)
	{
		dwSize = GetFileSize(h, 0);

		hMap = CreateFileMapping(h, 0, PAGE_READONLY, 0, dwSize, 0);
		if (hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_READ, 0, 0, dwSize);
		}
		else
		{
			return false;
		}
	}
	else
	{
		// h == IHV
		return false;
	}
	BYTE *bMem = (BYTE*)pMap;

	//_tcsncpy_s(result, dwSize + sizeof(TCHAR), (TCHAR*)(bCurMem), dwSize + sizeof(TCHAR));
	//return result;
	memcpy_s(result, dwSize, bMem, dwSize);
	return result;
}

void writeResult(BYTE* secondFile, int* foundIndexes, DWORD linesNum)
{
	int lineLen = 17;
	int lineSize = lineLen;
	int shortLineSize = (lineLen - 2);

	HANDLE resultFile = INVALID_HANDLE_VALUE;
	resultFile = CreateFile(_T("d:\\lab5\\3.txt"), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
		0, CREATE_ALWAYS, 0, 0);
	bool b = true;
	int resultSize = 64 * 1024 * sizeof(BYTE);
	BYTE* result = (BYTE*)malloc(resultSize);

	DWORD dwCount = 0;

	HANDLE hMap;
	LPVOID pMap;
	DWORD dwSize = linesNum * 50;

	if (resultFile != INVALID_HANDLE_VALUE)
	{
		hMap = CreateFileMapping(resultFile, 0, PAGE_READWRITE, 0, dwSize, 0);
		if (hMap)
		{
			pMap = MapViewOfFile(hMap, FILE_MAP_ALL_ACCESS, 0, 0, dwSize);
			BYTE *bMem = (BYTE*)pMap;
			BYTE *bCurMem = bMem;

			for (int i = 0; i < linesNum; i++)
			{
				memcpy(bCurMem, secondFile + i * lineSize, shortLineSize);
				bCurMem += shortLineSize;
				if (foundIndexes[i] == -1)
				{
					// Is not found.
					memcpy_s(bCurMem, 15, (BYTE*)(" is NOT FOUND\r\n"), 15);
					bCurMem += 15;
				}
				else
				{
					// Found.
					char* tail = (char*)malloc(50);
					int indexLength = 0;
					int tempValue = foundIndexes[i];
					while (tempValue > 0)
					{
						indexLength++;
						tempValue /= 10;
					}
					int t = sprintf_s(tail, 50, " is found at index   %i\r\n", foundIndexes[i]);
					//int t = strlen(tail);
					memcpy_s(bCurMem, t, (BYTE*)tail, t);
					bCurMem += t;
					free(tail);
				}
			}
		}
	}
}

void runConsistent()
{
	int lineLen = 17;
	int lineSize = lineLen;

	HANDLE file1 = openFile(1);
	//TCHAR* firstText = readFile(file1);
	BYTE* firstText = readFile(file1, 50 * 50);
	DWORD dwFileSize = GetFileSize(file1, 0);
	DWORD dwLinesNum1 = dwFileSize / lineLen;

	HANDLE file2 = openFile(2);
	//TCHAR* secondText = readFile(file2);
	BYTE* secondText = readFile(file2, 1000000 * 50);
	dwFileSize = GetFileSize(file2, 0);
	DWORD dwLinesNum2 = dwFileSize / lineLen;

	int* foundIndexes = (int*) malloc(dwLinesNum2 * sizeof(int));
	for (int i = 0; i < dwLinesNum2; i++)
	{
		foundIndexes[i] = -1;
	}
	// Fill indexes list
	int q = 0;
	while (firstText[q] != '\n')
	{
		q++;
	}
	firstText += q + 1;
	for (int i = 0; i < dwLinesNum2; i++)
	{
		// foreach line in second file

		int start = 0;
		int end = dwLinesNum1;
		while (start <= end)
		{
			int midle = (start + (end - start) / 2);
			int cmpRes = memcmp(secondText + i * lineSize, firstText + midle * lineSize, lineSize);
			if (cmpRes == 0)
			{
				// Found equal line.
				foundIndexes[i] = midle;
				break;
			}
			else if (cmpRes > 0)
			{
				if (start == midle)
				{
					break;
				}
				start = midle;
			}
			else
			{
				if (end == midle)
				{
					break;
				}
				end = midle;
			}
		}
	}
	
	writeResult(secondText, foundIndexes, dwLinesNum2);
	free(foundIndexes);
	//free(firstText);
	//free(secondText);


	/*printf("%s", firstText);
	printf("\n");
	printf("\n");
	printf("%s", secondText);*/
}

void runParallel()
{
	int lineLen = 17;
	int lineSize = lineLen;

	HANDLE file1 = openFile(1);
	//TCHAR* firstText = readFile(file1);
	BYTE* firstText = readFile(file1, 50 * 50);
	DWORD dwFileSize = GetFileSize(file1, 0);
	DWORD dwLinesNum1 = dwFileSize / lineLen;

	HANDLE file2 = openFile(2);
	//TCHAR* secondText = readFile(file2);
	BYTE* secondText = readFile(file2, 1000000 * 50);
	dwFileSize = GetFileSize(file2, 0);
	DWORD dwLinesNum2 = dwFileSize / lineLen;

	int* foundIndexes = (int*)malloc(dwLinesNum2 * sizeof(int));
#pragma omp parallel for
	for (int i = 0; i < dwLinesNum2; i++)
	{
		foundIndexes[i] = -1;
	}
	//
	int q = 0;
	while (firstText[q] != '\n')
	{
		q++;
	}
	firstText += q + 1;
	//
#pragma omp parallel for
	for (int i = 0; i < dwLinesNum2; i++)
	{
		// foreach line in second file

		int start = 0;
		int end = dwLinesNum1;
		while (start <= end)
		{
			int midle = (start + (end - start) / 2);
			int cmpRes = memcmp(secondText + i * lineSize, firstText + midle * lineSize, lineSize);
			if (cmpRes == 0)
			{
				// Found equal line.
				foundIndexes[i] = midle;
				break;
			}
			else if (cmpRes > 0)
			{
				if (start == midle)
				{
					break;
				}
				start = midle;
			}
			else
			{
				if (end == midle)
				{
					break;
				}
				end = midle;
			}
		}
	}

	writeResult(secondText, foundIndexes, dwLinesNum2);
	free(foundIndexes);
}

int _tmain(int argc, _TCHAR* argv[])
{
	int TESTS_NUMBER = 10;
	__int64 start = 0, finish = 0;
	__int64 diffSimple = MAXINT64;
	__int64 diffOMP = MAXINT64;
	__int64 frequency;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);

	for (int t = 0; t < TESTS_NUMBER; t++) {
		QueryPerformanceCounter((LARGE_INTEGER *)&start);
		runConsistent();
		QueryPerformanceCounter((LARGE_INTEGER *)&finish);
		if (finish - start < diffSimple) {
			diffSimple = finish - start;
		}
	}
	printf("Consistent. time==>%f ms\n", diffSimple * 1000.0 / frequency);

	for (int t = 0; t < TESTS_NUMBER; t++) {
		QueryPerformanceCounter((LARGE_INTEGER *)&start);
		runParallel();
		QueryPerformanceCounter((LARGE_INTEGER *)&finish);
		if (finish - start < diffOMP) {
			diffOMP = finish - start;
		}
	}
	printf("Parallel. time==>%f ms\n", diffOMP * 1000.0 / frequency);



	printf("asdf");
	getchar();

	return 0;
}

